<?php

/**
 * @file
 * This file contains the functions that we need to manage the Kraftwagen
 * archive.
 */

/**
 * Make sure we have a location to store an archived file and/or directory.
 *
 * @param string $name
 *   The name of the item to archive.
 * @param string $path
 *   The path to the item to archive.
 *
 * @return mixed
 *   The path where the item should be archived to, or FALSE on error.
 */
function kraftwagen_archive_prepare_location($name, $path) {
  $root = kraftwagen_context_get_option('root-path');

  $dir_archive = kraftwagen_context_get_option('archive-dir');

  if (!$dir_archive) {
    return drush_set_error(dt(
      'Could not archive !name from !path, because we could find the archive directory.',
      _kw_archive_t_args($name, $path)
    ));
  }

  if (!is_dir($root . DIRECTORY_SEPARATOR . $dir_archive)) {
    if (!is_writable($root)) {
      return drush_set_error(dt(
        'Could not archive !name from !path, because the archive directory !dir is not writable.',
        array('!dir' => $root) + _kw_archive_t_args($name, $path)
      ));
    }

    if (!drush_mkdir($root . DIRECTORY_SEPARATOR . $dir_archive)) {
      return drush_set_error(dt(
        'Could not archive !name from !path, because the archive directory !dir could not be created.',
        array('!dir' => $root . DIRECTORY_SEPARATOR . $dir_archive) +
          _kw_archive_t_args($name, $path)
      ));
    }

    // Notice, usually not visible.
    drush_log(dt(
      'Directory !dir created to archive !name.',
      array('!dir' => $root . DIRECTORY_SEPARATOR . $dir_archive) +
        _kw_archive_t_args($name, $path)
    ));
  }

  if (!is_dir($root . DIRECTORY_SEPARATOR . $dir_archive . DIRECTORY_SEPARATOR . $name)) {
    if (!is_writable($root . DIRECTORY_SEPARATOR . $dir_archive)) {
      return drush_set_error(dt(
        'Could not archive !name from !path, because the archive directory !archive is not writable.',
        array('!dir' => $root . DIRECTORY_SEPARATOR . $dir_archive) +
          _kw_archive_t_args($name, $path)
      ));
    }

    if (!drush_mkdir($root . DIRECTORY_SEPARATOR . $dir_archive . DIRECTORY_SEPARATOR . $name)) {
      return drush_set_error(dt(
        'Could not archive !name from !path, because the directory !dir could not be created.',
        array(
          '!dir' => $root . DIRECTORY_SEPARATOR . $dir_archive . DIRECTORY_SEPARATOR . $name,
        ) + _kw_archive_t_args($name, $path)
      ));
    }

    // Notice, usually not visible.
    drush_log(dt(
      'Directory !dir created to archive !name.',
      array(
        '!dir' => $root . DIRECTORY_SEPARATOR . $dir_archive . DIRECTORY_SEPARATOR . $name,
      ) + _kw_archive_t_args($name, $path)
    ));
  }

  if (!is_writable($root . DIRECTORY_SEPARATOR . $dir_archive . DIRECTORY_SEPARATOR . $name)) {
    return drush_set_error(dt(
      'Could not archive !name from !path, because the archive directory !dir is not writable.',
      array(
        '!dir' => $root . DIRECTORY_SEPARATOR . $dir_archive . DIRECTORY_SEPARATOR . $name,
      ) + _kw_archive_t_args($name, $path)
    ));
  }

  if (!($date_pattern = kraftwagen_context_get_option('date-pattern'))) {
    return drush_set_error(dt(
      'Could not archive !name from !path, because we have no date pattern set',
       _kw_archive_t_args($name, $path)
    ));
  }

  $date = date($date_pattern);

  $suffix = 0;
  do {
    $target = $root . DIRECTORY_SEPARATOR . $dir_archive . DIRECTORY_SEPARATOR . $name . DIRECTORY_SEPARATOR . $date . ($suffix ? "-{$suffix}" : '') . '-' . $name;
    $suffix += 1;
  } while (@lstat($target));

  return $target;
}

/**
 * Archive a file.
 *
 * @param string $name
 *   The name of the file to archive.
 * @param string $path
 *   The path to the file to archive.
 *
 * @return mixed
 *   The path where the file has been archived to, or FALSE on error.
 */
function kraftwagen_archive_file($name, $path) {
  if (!($target = kraftwagen_archive_prepare_location($name, $path))) {
    return $target;
  }

  if (drush_op('copy', $path, $target) !== TRUE) {
    return drush_set_error(dt(
      'Could not archive !name from !path to !target',
      array('!target' => $target) + _kw_archive_t_args($name, $path)
    ));
  }

  return $target;
}

/**
 * Archive a directory.
 *
 * @param string $name
 *   The name of the directory to archive.
 * @param string $path
 *   The path to the directory to archive.
 *
 * @return mixed
 *   The path where the directory has been archived to, or FALSE on error.
 */
function kraftwagen_archive_directory($name, $path) {
  if (!($target = kraftwagen_archive_prepare_location($name, $path))) {
    return $target;
  }

  if (drush_op('drush_copy_dir', $path, $target) !== TRUE) {
    return drush_set_error(dt(
      'Could not archive !name from !path to !target',
      array('!target' => $target) + _kw_archive_t_args($name, $path)
    ));
  }

  return $target;
}

/**
 * Internal helper function to construct t() arguments.
 */
function _kw_archive_t_args($name, $path) {
  return array('!name' => $name, '!path' => $path);
}
