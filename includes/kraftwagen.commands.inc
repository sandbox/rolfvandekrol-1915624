<?php

/**
 * @file
 * This file contains the helper functions to run command sequences.
 */

/**
 * Run a sequence of drush commandsm, defined in a Kraftwagen context option.
 *
 * @param string $option
 *   The context option where the sequence is defined.
 * @param array $replacements
 *   The array of parameter replacements.
 * @param bool $fail_on_error
 *   If TRUE, the sequence will halt when one command fails. Defaults to FALSE.
 *
 * @return bool
 *   FALSE on failure. If everything went well, NULL.
 */
function kraftwagen_commands_sequence_run($option, $replacements = array(), $fail_on_error = TRUE) {
  $commands = kraftwagen_context_get_option($option);
  $commands = kraftwagen_commands_sequence_replace($commands, $replacements);
  return kraftwagen_commands_sequence_run_commands($commands, $fail_on_error);
}

/**
 * Run a sequence of drush commands.
 *
 * @param array $commands
 *   The sequence of commands that should be executed.
 * @param bool $fail_on_error
 *   If TRUE, the sequence will halt when one command fails. Defaults to FALSE.
 *
 * @return bool
 *   FALSE on failure. If everything went well, NULL.
 */
function kraftwagen_commands_sequence_run_commands($commands, $fail_on_error = TRUE) {
  foreach ($commands as $command => $arguments) {
    if (is_numeric($command)) {
      if (is_array($arguments)) {
        kraftwagen_commands_sequence_run_commands($arguments);
        continue;
      }
      else {
        $command = $arguments;
        $arguments = array();
      }
    }

    $phase_index = drush_get_context('DRUSH_BOOTSTRAP_PHASE');
    if ($phase_index >= DRUSH_BOOTSTRAP_DRUPAL_FULL) {
      _drush_find_commandfiles(DRUSH_BOOTSTRAP_DRUPAL_FULL);
    }
    $available_commands = drush_get_commands();

    if (isset($available_commands[$command])) {
      $result = drush_invoke_process('@self', $command, $arguments);
      if (!$result || !empty($result['error_status'])) {
        $return = drush_set_error(dt("Called command !command returned an error.", array('!command' => $command)));
        if ($fail_on_error) {
          return $return;
        }
      }
    }
    else {
      $return = drush_set_error(dt("Command !command not found", array('!command' => $command)));
      if ($fail_on_error) {
        return $return;
      }
    }
  }
}

/**
 * Replace the arguments in the command sequence.
 *
 * @param mixed $data
 *   If array, run recusively over both the keys and values. If string, return
 *   that string replaced.
 * @param array $replacements
 *   The array of parameter replacements.
 *
 * @return mixed
 *   The replaced input.
 */
function kraftwagen_commands_sequence_replace($data, $replacements) {
  if (is_array($data)) {
    $result = array();
    foreach ($data as $key => $value) {
      $key = kraftwagen_commands_sequence_replace($key, $replacements);
      $value = kraftwagen_commands_sequence_replace($value, $replacements);
      $result[$key] = $value;
    }
    return $result;
  }
  elseif (is_string($data)) {
    return str_replace(array_keys($replacements), array_values($replacements), $data);
  }
  else {
    return $data;
  }
}
